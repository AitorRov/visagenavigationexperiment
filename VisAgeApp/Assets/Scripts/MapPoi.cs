﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class MapPoi : MonoBehaviour 
{
	private POI poi;
	public POI Poi
	{
		get { return poi; }
		set 
		{ 
			poi = value;
			this.name = "BtnPoi " + poi.Name;
		}
	}

	private GameObject player;
	public  GameObject Player
	{
		get {return player;}
		set { player = value;}
	}

	private bool takeMeThere = false;
	public bool  TakeMeThere
	{
		get { return takeMeThere;}
		set { takeMeThere = value;}
	}

	GameObject panel;
	private bool showPanel=false;
	public void ShowPanel()
	{
		this.transform.parent.gameObject.GetComponent<MapUI>().HidePanels();
		showPanel=true;
		panel.SetActive (showPanel);
	}
	public void HidePanel(){ showPanel=false; 		if(panel!=false)panel.SetActive (showPanel);}
	public void SwitchPanel()
	{ 	
		if(showPanel)
		{
			HidePanel ();
		}
		else
		{
			ShowPanel();
		}
	}		
	
	Experiment01EventHandler experiment01handler;
	
	public void ShowTrackerPreview()
	{
		if(Application.loadedLevelName == "ExperimentNavigation")
		{
			//arp experiment navigation. One semi transparent panel
			GameObject go = GameObject.Find ("Experiment01Handler");
			if(go!=null) 
				experiment01handler = go.GetComponent<Experiment01EventHandler>();	
					
			experiment01handler.TrackerPreviewShow(poi);
		}			
		else
		{
			//GameObject PanelLoc = GameObject.Find ("PanelLoc");
			//GameObject PanelImage = GameObject.Find ("PanelLoc/Scroller/ScrollerContent/Image");
			
			//PanelLoc.transform.SetParent(transform);

			////Text PanelAddress = GameObject.Find (this.name+"/Panel/Scroller/ScrollerContent/Address").GetComponent<Text>();
			////PanelAddress.text = poi.Name;
			//string ImagePath = Path.Combine(Application.persistentDataPath,SceneTools.AreaNameDefault()+"/StreamingAssets/"+poi.ImageTarget);
			//Image im = PanelImage.GetComponent<Image>();
			//Texture2D tex = new Texture2D(256,256);
			//tex.LoadImage(System.IO.File.ReadAllBytes(ImagePath));
			//Sprite sprite = new Sprite();
			//sprite = Sprite.Create (tex,new Rect(0, 0, tex.width, tex.height),new Vector2());
			//im.sprite = sprite;
		}
 }
	

//	public void Update()
//	{
//		if (takeMeThere) 
//		{
//			if(nodes.Length == 0)
//			{
////				string p1 = player;
////				string p2[] = {poi.long+","+poi.la}
////				Directions directions = Directions.GetDirections(p1,p2);
//			}
//		}
//	}


}
