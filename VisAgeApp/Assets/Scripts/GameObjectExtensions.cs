﻿using System;
using UnityEngine;
using System.Collections;

public static class GameObjectExtensions 
{

	public static GameObject Parent (this GameObject go)
	{
		return go.transform.parent.gameObject;
	}
	
	public static string GetPathInHierarchy(this GameObject go) 
	{
		if (go.transform.parent == null)
			return "/" + go.name;
		
		return go.Parent ().GetPathInHierarchy() + "/" + go.name;
	}
	
	public static GameObject FindChild(this GameObject root, string pName)
	{
		//Returns the first ocurrence of a child GameObject called pName using depth search first
		GameObject ret;
		
		//Debug.Log("Finding child in " + root.transform);
		
		foreach (Transform transform in root.transform) 
		{
			if(!transform.gameObject.activeSelf) continue;
			
			if (transform.name == pName)
			{
				//Debug.Log(pName + " found."); 
				return transform.gameObject;
			}
			
			else ret = FindChild(transform.gameObject, pName);
			
			if (ret != null) return ret;			
		}
		
		return null;			
	}
}
