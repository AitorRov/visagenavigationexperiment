﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Collections.Generic;

[XmlRoot("response")]
public class Directions
{

	public Info info;
	public class Info
	{
		public int 			statusCode;
		[XmlArray("messages"),XmlArrayItem("message")]
		public string[] 	messages;
		public Copyright 	copyright;
		
		public class Copyright
		{
			public string 	imageUrl;
			public string 	imageAltText;
			public string	text;
		}
		
	}
	
	public RouteError routeError;
	public class RouteError
	{
		public int errorCode;
		public string message;	
	}
	
	public Route route;
	public class Route
	{
		public string sessionId;
		
		public Options options;
		public class Options
		{
			public string   shapeFormat;
			public float	generalize;
			public int		maxLinkId;
			public string	narrativeType;
			public bool 	stateBoundaryDisplay;
			public bool		countryBoundaryDisplay;
			public bool 	sideOfStreetDisplay;
			public bool 	destinationManeuverDisplay;
			public bool 	avoidTimedConditions;
			public bool		enhancedNarrative;
			public bool 	returnLinkDirections;
			public int		timeType;
			public string 	routeType;
			public string 	locale;
			public string	unit;
			public int[]	tryAvoidLinkIds; //not sure about type
			public int[] 	mustAvoidLinkIds; //not sure about type
			public bool		manmaps;
			public int		drivingStyle;
			public float	highwayEfficiency;
			public bool		useTraffic;
		}
	
		public BoundingBox boundingBox;
		public class BoundingBox
		{
			public LatLng ul;
			public LatLng lr;
		}
		
		public float 	distance;
		public int 		time;
		public int 		realTime;
		public float 	fuelUsed;
		public string 	formattedTime;
	
		[XmlArray("legs"),XmlArrayItem("leg")]
		public Leg[] legs;
	
		[XmlArray("locations"),XmlArrayItem("location")]
		public Location[]   locations;
	
		public string locationSequence;
		[XmlArray("computedWaypoints"),XmlArrayItem("computedWaypoint")] //not sure
		public string[] computedWaypoints;
	}//class Route

	
	public static Directions GetDirections(string From, string[] To)
	{
		string input = "http://open.mapquestapi.com/directions/v1/route?key=" + SceneTools.MapQuestKey () +
			"&from=" + From.Replace (" ", "%20");
			
		foreach(string s in To)
			input = input + "&to=" + s.Replace (" ", "%20");
			
		input = input +	"&routeType=pedestrian" + 	 
			"&locale=en_GB" + 
			"&unit=k" +
			"&outFormat=xml"; 
			
		string output = Path.Combine (Application.persistentDataPath, "directions-"+From.Replace (" ","")+"-"+To[0].Replace (" ", "")+".xml");
		Debug.Log (input);
		//Debug.Log ("Route output file: " + output);
		
		//for debug purposes, don't download again
		//if(!File.Exists(output))
		SceneTools.DownloadFileFromUrlSync (input, output);			
			
		Directions directions = Open(output);
		return directions;
	}
	
	public static Directions Open (string file)
	{
		Directions directions = new Directions ();
		var serializer = new XmlSerializer(typeof(Directions));
		
		using(var stream = new FileStream(Path.Combine (Application.dataPath, file), FileMode.Open))
		{
			directions = serializer.Deserialize(stream) as Directions;
		}
		
		return directions;
	}	
	
	public bool IsValid()
	{
		if(info.statusCode != 0)
		{
			foreach (string m in info.messages)
			{
				Debug.LogWarning(info.statusCode + ": " + m);
			}
			
			return false;
		}
		else
		{	
			return true;		
		}
	
	}
	
	public List<LatLng> GetControlpoints()
	{
		List<LatLng> controlPoints = new List<LatLng>();

		//first add the origin
		controlPoints.Add(route.locations[0].latLng);
				
		foreach(Leg leg in route.legs)
			foreach(Maneuver manv in leg.maneuvers)
			{
				if(manv.startPoint.lng != 0 && manv.startPoint.lat != 0) //not sure why there is an empty control point in the end
						controlPoints.Add(manv.startPoint);
			}		
		
		//last point is destination
		controlPoints.Add(route.locations[route.locations.Length - 1].latLng);
		
		return controlPoints;
	
	}
	
	public List<string> GetNarrative()
	{
		List<string> narrative = new List<string>();
		
		foreach(Leg leg in route.legs)
			foreach(Maneuver manv in leg.maneuvers)
		{
				narrative.Add(manv.narrative);
		}		
		
		return narrative;		
		
	}
	public string GetNarrativeString() 
	{
		string output = "";
		
		List<string> narrative = GetNarrative();
		
		foreach(string n in narrative)
		{
			output = output + "- " + n + "\n";
		}	
		
		return output;
	}
	
	
	public Object GetXmlSubTree(string descendant)
	{
		//this piece of code works. It's for reading a subtree in the XML structure
		//not sure the return type of this function though
		//		XmlReader reader = XmlReader.Create (Path.Combine (Application.persistentDataPath, "query.xml"));
		//		reader.ReadToDescendant(descendant);
		//		XmlSerializer  serializer = new XmlSerializer(Type.GetType(descendant));//typeof(Route.Guidance.GuidanceNodeCollection));
		//
		//		XmlReader inner = reader.ReadSubtree ();
		//		Route.Info info = serializer.Deserialize(inner) as Route.Info; 
		//		reader.Close();
		
		return 	null;
	}
	
}//class Directions

public class Leg
{
	public float 	distance;
	public int 		time;		
	public string 	formattedTime;
	public int 		index;	
	
	[XmlArray("maneuvers"),XmlArrayItem("maneuver")]
	public Maneuver[] maneuvers;
	public bool hasTollRoad;
	public bool hasFerry;
	public bool hasHighway;
	public bool hasSeasonalClosure;
	public bool hasUnpaved;
	public bool hasCountryCross;
}

public class Maneuver
{
	public LatLng 	startPoint;
	public string 	maneuverNotes;
	public float  	distance;
	public int 		time;		
	public string 	formattedTime;	
	public string 	attributes; //not sure
	public int 		turnType;
	public int		direction;
	public string 	narrative;
	public string	directionName;
	public int		index;
	[XmlArray("streets"),XmlArrayItem("street")]
	public string[] streets;
	[XmlArray("signs"),XmlArrayItem("sign")]
	public string[] signs;
	public string 	iconUrl;
	[XmlArray("linkIds"),XmlArrayItem("linkId")] //not sure
	public int[]	linkIds;
	public string 	mapUrl;
}

public class Location
{
	public string street;
	public string adminArea5; //attributes not implemented in all adminArea fields
	public string adminArea3;
	public string adminArea4;
	public string postalCode;
	public string adminArea1;
	public string geocodeQuality;
	public string geocodeQualityCode;
	public bool   dragPoint;
	public string sideOfStreet;
	public DisplayLatLng displayLatLng;
	public class DisplayLatLng
	{
		LatLng latLng; //as it is defined in the xml file, with double tag encapsulation. Weird.
		//it doesn't even work, not sure why.
	}
	public int 	  linkId;
	public string type;
	public LatLng latLng;
}

public class LatLng
{
	public double lat;
	public double lng;
}






