﻿using System;
//using ProjNet.CoordinateSystems;
//using ProjNet.CoordinateSystems.Transformations;
//using ProjNet.Converters.WellKnownText;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySlippyMap;
using metaio;

public class SceneVisage : MonoBehaviour
{
	private Map		map;
	public Map theMap
	{
		get
		{
			return map;
		}
	}
	
	public Texture   PlayerTexture;
	public Texture   PlayerAccuracyTexture;
	//public Material  PlayerAccuracyMaterial;
	//public Texture	PoiTexture;
	
	public bool		ShowOldGUI;
	
	private float	guiXScale;
	private float	guiYScale;
	private Rect	guiRect;
	
	//private bool 	isPerspectiveView = false;
	//private float	perspectiveAngle = 30.0f;
	//private float	destinationAngle = 0.0f;
	//private float	currentAngle = 0.0f;
	//private float	animationDuration = 0.5f;
	//private float	animationStartTime = 0.0f;
	
	private List<Layer> layers;
	private int     currentLayerIndex = 0;
	
	//arp 
	public Area area;
	bool modeAR = true;
	Camera CameraMap;
	Camera CameraAR;
	public Rect bigRect = new Rect(0f,0f,1f,1f); 
	public Rect smallRect = new Rect(0.75f,0.75f,0.2f,0.2f); 	
	bool TouchPhaseBegan = false;
	
	bool Toolbar(Map map)
	{
		if(!ShowOldGUI) return false;
		
		GUI.matrix = Matrix4x4.Scale(new Vector3(guiXScale, guiXScale, 1.0f));
		
		GUILayout.BeginArea(guiRect);
		
		GUILayout.BeginHorizontal();
		
		//GUILayout.Label("Zoom: " + map.CurrentZoom);
		
		bool pressed = false;
		if (GUILayout.RepeatButton("+", GUILayout.ExpandHeight(true)))
		{
			map.Zoom(1.0f);
			pressed = true;
		}
		if (Event.current.type == EventType.Repaint)
		{
			Rect rect = GUILayoutUtility.GetLastRect();
			if (rect.Contains(Event.current.mousePosition))
				pressed = true;
		}
		
//		if (GUILayout.Button("2D/3D", GUILayout.ExpandHeight(true)))
//		{
//			if (isPerspectiveView)
//			{
//				destinationAngle = -perspectiveAngle;
//			}
//			else
//			{
//				destinationAngle = perspectiveAngle;
//			}
//			
//			animationStartTime = Time.time;
//			
//			isPerspectiveView = !isPerspectiveView;
//		}
		if (Event.current.type == EventType.Repaint)
		{
			Rect rect = GUILayoutUtility.GetLastRect();
			if (rect.Contains(Event.current.mousePosition))
				pressed = true;
		}
		
		if (GUILayout.Button("Center", GUILayout.ExpandHeight(true)))
		{
			map.CenterOnLocation();
		}
		if (Event.current.type == EventType.Repaint)
		{
			Rect rect = GUILayoutUtility.GetLastRect();
			if (rect.Contains(Event.current.mousePosition))
				pressed = true;
		}
		
		string layerMessage = String.Empty;
		if (map.CurrentZoom > layers[currentLayerIndex].MaxZoom)
			layerMessage = "\nZoom out!";
		else if (map.CurrentZoom < layers[currentLayerIndex].MinZoom)
			layerMessage = "\nZoom in!";
		if (GUILayout.Button(((layers != null && currentLayerIndex < layers.Count) ? layers[currentLayerIndex].name + layerMessage : "Layer"), GUILayout.ExpandHeight(true)))
		{
			#if UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_3_6 || UNITY_3_7 || UNITY_3_8 || UNITY_3_9
			layers[currentLayerIndex].gameObject.SetActiveRecursively(false);
			#else
			layers[currentLayerIndex].gameObject.SetActive(false);
			#endif
			++currentLayerIndex;
			if (currentLayerIndex >= layers.Count)
				currentLayerIndex = 0;
			#if UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_3_6 || UNITY_3_7 || UNITY_3_8 || UNITY_3_9
			layers[currentLayerIndex].gameObject.SetActiveRecursively(true);
			#else
			layers[currentLayerIndex].gameObject.SetActive(true);
			#endif
			map.IsDirty = true;
		}
		
		if (GUILayout.RepeatButton("-", GUILayout.ExpandHeight(true)))
		{
			map.Zoom(-1.0f);
			pressed = true;
		}
		if (Event.current.type == EventType.Repaint)
		{
			Rect rect = GUILayoutUtility.GetLastRect();
			if (rect.Contains(Event.current.mousePosition))
				pressed = true;
		}
		
		GUILayout.EndHorizontal();
		
		GUILayout.EndArea();
		
		return pressed;
	}
	
	private
		#if !UNITY_WEBPLAYER
		IEnumerator
			#else
			void
			#endif
			Start()
	{
		// setup the gui scale according to the screen resolution
		guiXScale = (Screen.orientation == ScreenOrientation.Landscape ? Screen.width : Screen.height) / 480.0f;
		guiYScale = (Screen.orientation == ScreenOrientation.Landscape ? Screen.height : Screen.width) / 640.0f;
		// setup the gui area
		guiRect = new Rect(16.0f * guiXScale, 4.0f * guiXScale, Screen.width / guiXScale - 32.0f * guiXScale, 32.0f * guiYScale);
		
		// create the map singleton
		map = Map.Instance;
		//arp camera
		if(SceneTools.IsNewSceneDesign())
		{
			CameraMap = GameObject.Find ("CameraMap").GetComponent<Camera>();
			//CameraMap.cullingMask |= (1 << LayerMask.NameToLayer(SceneTools.ContentLayerMaskName()));
			//CameraMap.cullingMask &= ~(1 << LayerMask.NameToLayer(SceneTools.ContentLayerMaskName())); //don't render objects in VisAgeContent layer using the CameraMap
			//actually, it's aldready set up on the GameObjet parameters.
			map.CurrentCamera = CameraMap;
			
			CameraAR = GameObject.Find ("metaioSDK/DeviceCamera").GetComponent<Camera>();
		}
		else
			map.CurrentCamera = Camera.main;
		//end arp
		
		map.InputDelegate += UnitySlippyMap.Input.MapInput.BasicTouchAndKeyboard;
		map.CurrentZoom = 17.0f; //arp change zoom 15.0f;
		
		map.CenterWGS84 = SceneTools.DefaultGPSlocationForSlippyMap(); //new double[2] { -0.1322190,51.5231850 };
		map.UseLocation = true;
		map.InputsEnabled = true;
		map.ShowGUIControls = true;
		
		map.GUIDelegate += Toolbar;
		
		layers = new List<Layer>();
		
		// create an OSM tile layer
		OSMTileLayer osmLayer = map.CreateLayer<OSMTileLayer>("OSM");
		osmLayer.BaseURL = "http://a.tile.openstreetmap.org/";
		
		layers.Add(osmLayer);
		
		// create a WMS tile layer
		WMSTileLayer wmsLayer = map.CreateLayer<WMSTileLayer>("WMS");
		//wmsLayer.BaseURL = "http://129.206.228.72/cached/osm?"; // http://www.osm-wms.de : seems to be of very limited use
		//wmsLayer.Layers = "osm_auto:all";
		wmsLayer.BaseURL = "http://vmap0.tiles.osgeo.org/wms/vmap0";
		wmsLayer.Layers = "basic";
		#if UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_3_6 || UNITY_3_7 || UNITY_3_8 || UNITY_3_9
		wmsLayer.gameObject.SetActiveRecursively(false);
		#else
		wmsLayer.gameObject.SetActive(false);
		#endif
		
		layers.Add(wmsLayer);
		
		// create a VirtualEarth tile layer
		VirtualEarthTileLayer virtualEarthLayer = map.CreateLayer<VirtualEarthTileLayer>("VirtualEarth");
		// Note: this is the key UnitySlippyMap, DO NOT use it for any other purpose than testing
		//virtualEarthLayer.Key = "ArgkafZs0o_PGBuyg468RaapkeIQce996gkyCe8JN30MjY92zC_2hcgBU_rHVUwT";
		#if UNITY_WEBPLAYER
		virtualEarthLayer.ProxyURL = "http://reallyreallyreal.com/UnitySlippyMap/demo/veproxy.php";
		#endif
		#if UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_3_6 || UNITY_3_7 || UNITY_3_8 || UNITY_3_9
		virtualEarthLayer.gameObject.SetActiveRecursively(false);
		#else
		virtualEarthLayer.gameObject.SetActive(false);
		#endif
		
		layers.Add(virtualEarthLayer);
		
		#if !UNITY_WEBPLAYER // FIXME: SQLite won't work in webplayer except if I find a full .NET 2.0 implementation (for free)
		// create an MBTiles tile layer
		bool error = false;
		// on iOS, you need to add the db file to the Xcode project using a directory reference
		string mbTilesDir = "MBTiles/";
		string filename = "UnitySlippyMap_World_0_8.mbtiles";
		string filepath = null;
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			filepath = Application.streamingAssetsPath + "/" + mbTilesDir + filename;
		}
		else if (Application.platform == RuntimePlatform.Android)
		{
			// Note: Android is a bit tricky, Unity produces APK files and those are never unzip on the device.
			// Place your MBTiles file in the StreamingAssets folder (http://docs.unity3d.com/Documentation/Manual/StreamingAssets.html).
			// Then you need to access the APK on the device with WWW and copy the file to persitentDataPath
			// to that it can be read by SqliteDatabase as an individual file
			string newfilepath = Application.temporaryCachePath + "/" + filename;
			if (File.Exists(newfilepath) == false)
			{
				Debug.Log("DEBUG: file doesn't exist: " + newfilepath);
				filepath = Application.streamingAssetsPath + "/" + mbTilesDir + filename;
				// TODO: read the file with WWW and write it to persitentDataPath
				WWW loader = new WWW(filepath);
				yield return loader;
				if (loader.error != null)
				{
					Debug.LogError("ERROR: " + loader.error);
					error = true;
				}
				else
				{
					Debug.Log("DEBUG: will write: '" + filepath + "' to: '" + newfilepath + "'");
					File.WriteAllBytes(newfilepath, loader.bytes);
				}
			}
			else
				Debug.Log("DEBUG: exists: " + newfilepath);
			filepath = newfilepath;
		}
		else
		{
			filepath = Application.streamingAssetsPath + "/" + mbTilesDir + filename;
		}
		
		if (error == false)
		{
			Debug.Log("DEBUG: using MBTiles file: " + filepath);
			MBTilesLayer mbTilesLayer = map.CreateLayer<MBTilesLayer>("MBTiles");
			mbTilesLayer.Filepath = filepath;
			#if UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_3_6 || UNITY_3_7 || UNITY_3_8 || UNITY_3_9
			mbTilesLayer.gameObject.SetActiveRecursively(false);
			#else
			mbTilesLayer.gameObject.SetActive(false);
			#endif
			
			layers.Add(mbTilesLayer);
		}
		else
			Debug.LogError("ERROR: MBTiles file not found!");
		
		#endif
		
		area = Area.Load();		
		CreateTrackerPois();
		CreateMapPois();
		CreatePlayer();
		//TestCreateRoutes();

		//arp
		string MetaioFileLocal = SceneTools.AreaTrackingDataFile();
		MetaioSDKUnity.setTrackingConfiguration (MetaioFileLocal,1);			
						
//		if(Application.isEditor) ShowOldGUI = true;
	}
	
	void CreateTrackerPois()
	{
		GameObject areaObject = new GameObject();
		areaObject.name = "[Trackers]";
		int i = 0;
		foreach (POI poi in area.POIs)
		{
			poi.Instantiate (areaObject,++i);
		}
	}	

	void CreateMapPois()
	{
		GameObject go = Tile.CreateTileTemplate("grob",Tile.AnchorPoint.BottomCenter).gameObject;
		//Debug only to check the markers are on the right spot. Do not render
		go.GetComponent<Renderer>().enabled = false;
		//go.renderer.material.mainTexture = PoiTexture;
		//go.renderer.material.renderQueue = 4001;
		go.transform.localScale = new Vector3(0.70588235294118f, 1.0f, 1.0f);
		go.transform.localScale /= 7.0f;
		//go.AddComponent<CameraFacingBillboard>().Axis = Vector3.up;

		GameObject BtnLoc = GameObject.Find ("BtnLoc");
		Transform  canvas = BtnLoc.transform.parent;
		GameObject markerUI;
		GameObject markerGO;
		
		int counter = 0;
		foreach (POI poi in area.POIs) 
		{
			markerGO = Instantiate(go) as GameObject;
			Marker marker = map.CreateMarker<Marker>(poi.Name, new double[2] { poi.Longitude,poi.Latitude }, markerGO);
			markerGO.name = poi.Name;
			
			//arp instantiate UI element
			markerUI = Instantiate(BtnLoc) as GameObject;
			markerUI.transform.SetParent(canvas);
			RectTransform rt = markerUI.GetComponent<RectTransform>();
			marker.MarkerUI = rt;

			markerUI.GetComponent<MapPoi>().Poi = poi;
			
			poi.SetMapObj(markerUI);

			counter++;
		}
		
		DestroyImmediate(go);	
		Debug.Log ("Pois created: " + counter);
	}	
	
	void CreatePlayer()
	{
	
		GameObject player = new GameObject();	
		player.name = "[Player]";		
		
		GameObject dot = Tile.CreateTileTemplate().gameObject;
		dot.name = "Dot";
		dot.transform.parent = player.transform;
		dot.GetComponent<Renderer>().material.mainTexture = PlayerTexture;
		dot.GetComponent<Renderer>().material.renderQueue = 4000;
		dot.transform.localScale /= 27.0f;
		
		GameObject markerGO = Instantiate(player) as GameObject;
		map.SetLocationMarker<LocationMarker>(markerGO);
		
		//if(!Application.isEditor) 			
			DestroyImmediate(player);

		GameObject player2 = new GameObject();	
		player2.name = "[Player]";				
						
		GameObject circle = Tile.CreateTileTemplate().gameObject;
		circle.name = "Circle";
		circle.transform.parent = player2.transform;
		circle.GetComponent<Renderer>().material.mainTexture = PlayerAccuracyTexture;
		circle.GetComponent<Renderer>().material.color = new Color(1f,1f,1f,0.4f);
		circle.GetComponent<Renderer>().material.renderQueue = 3999;
		circle.transform.localScale = new Vector3(0.3f,0.3f,0.3f);
		circle.AddComponent<MapPlayerCircle>();
	

		
//		if(!Application.isEditor) 			
//			DestroyImmediate(player);

	}		

	void Update(){
	
		Vector2 hitPoint = new Vector2();
#if UNITY_EDITOR		
		if(Input.GetMouseButtonDown(0)) hitPoint = Input.mousePosition;
#else		
		if (Input.touchCount == 1 /*&& Input.touches[0].TouchPhase == TouchPhase.Began*/) 
		{ 
			hitPoint = Input.touches[0].position;
		}
#endif		

		if(hitPoint != Vector2.zero && TouchPhaseBegan) //TouchPhaseBegan is a hack, since TouchPhase seems not to be recognized by the compiler
		{

			hitPoint = new Vector2(hitPoint.x/Screen.width , hitPoint.y/Screen.height);

			if(smallRect.Contains(hitPoint)) SwitchMode();	
			
			TouchPhaseBegan = false;
		}

		if(hitPoint == Vector2.zero)
		{
			TouchPhaseBegan = true;
		}
	}
		
	public void SwitchMode()
	{
		if(SceneTools.IsNewSceneDesign())
		{
			modeAR = !modeAR;
			
			if(modeAR){
				CameraAR.rect = bigRect;
				CameraAR.depth = -1;		
				CameraMap.rect = smallRect;
				CameraMap.depth = 0;
			}
			else{
				CameraAR.rect = smallRect;
				CameraAR.depth = 0; 
				CameraMap.rect = bigRect;
				CameraMap.depth = -1;				
			}

			metaioCamera[] cameras = (metaioCamera[])FindObjectsOfType(typeof(metaioCamera));
			foreach (metaioCamera camera in cameras)
			{
				camera.SwitchMode(modeAR,smallRect);
			}
		}
		else
		{
			Debug.Log ("loading AR scene");
			Application.LoadLevel("SceneAR");
		}
	}		
	
	

//	private AsyncOperation async = null; // When assigned, load is in progress.
//	
//	public IEnumerator LoadALevel() 
//	{
//		Debug.Log ("LoadALevel");
//		async = Application.LoadLevelAsync("sceneAR");
//		yield return async;
//	}
//	
//	void OnGUI() 
//	{	
//
//		if (async != null) 
//		{
//			Debug.Log ("loading");
//			GUI.DrawTexture(new Rect(0, 0, 500, 50), emptyProgressBar);
//			GUI.DrawTexture(new Rect(0, 0, 500 * async.progress, 50), fullProgressBar);
//		}
//	}	

	public Camera getCameraMap()
	{
		return CameraMap;
	}
	
	public bool GetModeAR()
	{
		return modeAR;	
	}
}



