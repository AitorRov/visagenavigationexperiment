﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnitySlippyMap;

public class MapPlayerCircle : MonoBehaviour 
{
	GameObject player;
	float haccuracy;
	float simaccuracy = 80f;
//	Text textdebug;

	float constant = 0.0025f; //adjustment to real distance
	
	void Start()
	{
		player = GameObject.Find ("[Map]/[Player](Clone)/[Player](Clone)");
		
//		if(GameObject.Find ("Canvas/TextDebug"))
//			textdebug = GameObject.Find ("Canvas/TextDebug").GetComponent<Text>();
	}
	
	void Update()
	{
		if(!player) 
			player = GameObject.Find ("[Map]/[Player](Clone)/[Player](Clone)");
		else
			transform.position = player.transform.position;
		
		
		if(UnityEngine.Input.location.status == LocationServiceStatus.Running)
		{
			haccuracy = Input.location.lastData.horizontalAccuracy * constant;
			//vaccuracy = Input.location.lastData.verticalAccuracy;
		}
		else
		{
			haccuracy = simaccuracy * constant;
			//vaccuracy = simaccuracy;
		}
		
		transform.localScale = new Vector3(haccuracy,haccuracy,haccuracy);
			
//		if(textdebug)
//		{			
//			string TheText = Input.location.lastData.horizontalAccuracy + "\n" ;							 
//			textdebug.text = TheText;
//    	}			
	
	}
	
}
