﻿using UnityEngine;
using System.Collections;

public class MessageBoard : MonoBehaviour 
{
	GameObject board;
	string message;

	void Start () 
	{
		board = GameObject.Find("Board");	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Resize ();
		
		//board.GetComponent<Text>().text = status;		
	}
	
	void Resize()
	{
		RectTransform a  = board.GetComponent<RectTransform>();
		
		a.sizeDelta = new Vector2(Screen.width * 0.8f,Screen.height * 0.25f);
		a.position = new Vector3(a.sizeDelta.x/2, a.sizeDelta.y/2,0) + new Vector3(Screen.width*0.1f,Screen.height*0.1f,0);
	}	
	
	public void SetMessage(string NewMessage)
	{
		message = NewMessage;
	}
	
	public void AppendMessage(string NewMessage)
	{
		message += "\n"+NewMessage;
	
	}
}
