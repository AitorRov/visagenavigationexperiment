﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class Tools 
{


//    [STAThread]
    public static void PrintCallStack()
    {
      StackTrace stackTrace = new StackTrace();           // get call stack
      StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

      // write call stack method names
      foreach (StackFrame stackFrame in stackFrames)
      {
        UnityEngine.Debug.Log(stackFrame.GetMethod().Name);   // write method name
      }
    }


}
