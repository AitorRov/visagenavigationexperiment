﻿using UnityEngine;
using System.Collections;
using UnitySlippyMap;

public class PoiLocationAdjust : MonoBehaviour 
{
	GameObject mapObj;
	int poiCounter = 0;
	POI poi;
	GameObject MapPoi;	
	Marker	   marker;
	
	float step = 0.0001f;
	
	void Start()
	{
		
	}
	
	
	void Update () 
	{
		if(!mapObj)
		{
			mapObj = GameObject.Find("[Map]");
			nextPoi ();
	
		}

		if (Input.GetKeyDown(KeyCode.Keypad1)) {step = step / 10; Debug.Log (step);}
		if (Input.GetKeyDown(KeyCode.Keypad7)) {step = step * 10; Debug.Log (step);}

		//up/down
		float oneStepUp = 0;
		if (Input.GetKeyDown(KeyCode.Keypad8)) {oneStepUp = step;}
		if (Input.GetKeyDown(KeyCode.Keypad2)) {oneStepUp = -step;}
		
		//left/right
		float oneStepLeft = 0;
		if (Input.GetKeyDown(KeyCode.Keypad4)) {oneStepLeft = -step;}
		if (Input.GetKeyDown(KeyCode.Keypad6)) {oneStepLeft = step;}

		if (Input.GetKeyDown(KeyCode.Keypad5)) nextPoi();

		if(oneStepUp != 0 || oneStepLeft != 0)
		{
			double[] coords = {marker.CoordinatesWGS84[0] + oneStepLeft, marker.CoordinatesWGS84[1] + oneStepUp};
			marker.CoordinatesWGS84 = coords;
			
			Debug.Log(marker.CoordinatesWGS84[0] + " " + marker.CoordinatesWGS84[1]);
		}
	}
	
	
	void nextPoi()
	{
		foreach (Transform t in mapObj.transform)
		{
			if(t.name.StartsWith("0" + poiCounter.ToString()))
			{
				MapPoi = t.gameObject;
				marker = MapPoi.GetComponent<Marker>();			
				Debug.Log (t.name);
				break;
			}
		}

		poiCounter = poiCounter + 1;
	}
}
