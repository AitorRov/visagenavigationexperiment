﻿//
// Radar - Based off of the other radar scripts I've seen on the wiki
// By: Justin Zaun

using UnityEngine;
using System.Collections;

public class Radar : MonoBehaviour
{
//    public enum RadarTypes : int { Textured, Round, Transparent };
//    public enum RadarLocations : int { TopLeft, TopCenter, TopRight, BottomLeft, BottomCenter, BottomRight, Left, Center, Right, Custom };

//    // Display Location
//    public RadarLocations 	radarLocation = RadarLocations.BottomCenter;
//    public Vector2 		  	radarLocationCustom;
//    public RadarTypes 		radarType = RadarTypes.Round;
//    public Color			radarBackgroundA = new Color(255, 255, 0);
//    public Color 			radarBackgroundB = new Color(0, 255, 255);
//    public Texture2D 		radarTexture;
//    public Texture2D 		centerIcon;
//    public float 			radarSize = 0.20f;  // The amount of the screen the radar will use
//    public float 			radarZoom = 0.60f;

//    // Center Object information
//    public bool radarCenterActive;
//    //public Color radarCenterColor = new Color(255, 255, 255);
//    //public string radarCenterTag;

//    // Blip information
//    public bool radarBlip1Active;
//    public Color radarBlip1Color = new Color(0, 0, 255);
//    public Color radarBlip1ColorOutOfBoundary = new Color(0, 255, 255);
//    public string radarBlip1Tag;

//    // Internal vars
//    //private GameObject _centerObject;
//    private int _radarWidth;
//    private int _radarHeight;
//    private Vector2 _radarCenter;
//    private Texture2D _radarCenterTexture;
//    private Texture2D _radarBlip1Texture;
//    private Texture2D _radarBlip1OoBTexture;

//    //Area area;
//    GPS gps;
//    GPSlocation UserLocation;
//    float 	    UserDirection;
//    float rotAngle;

//    //float CurrentRadarSize;
//    //float CurrentRadarZoom;
	
//    public float SimHeading = 0;
	
//    void Start()
//    {
//        // Determine the size of the radar
//        _radarWidth = (int)(Screen.width * radarSize);
//        _radarHeight = _radarWidth;

//        // Create the blip textures
//        //_radarCenterTexture = new Texture2D(3, 3, TextureFormat.RGB24, false);
//        _radarBlip1Texture = new Texture2D(3, 3, TextureFormat.RGB24, false);
//        _radarBlip1OoBTexture = new Texture2D(3, 3, TextureFormat.RGB24, false);

//        //CreateBlipTexture(_radarCenterTexture, radarCenterColor);
//        CreateBlipTexture(_radarBlip1Texture, radarBlip1Color);
//        CreateBlipTexture(_radarBlip1OoBTexture, radarBlip1ColorOutOfBoundary);

//        // Setup the radar background texture
//        if (radarType != RadarTypes.Textured)
//        {
//            radarTexture = new Texture2D(_radarWidth, _radarHeight, TextureFormat.RGB24, false);
//            CreateRoundTexture(radarTexture, radarBackgroundA, radarBackgroundB);
//        }

//        //area = GameObject.Find ("ARscene").GetComponent<SceneAR> ().getArea ();

//        // Get our center object        
////        GameObject[] gos;
////        gos = GameObject.FindGameObjectsWithTag(radarCenterTag);
////        _centerObject = gos[0];

//        gps = new GPS ();
//        gps.Start ();
		
//        Debug.Log ("Radar initialized");
//    }

//    // Update is called once per frame
//    void OnGUI()
//    {
//        // Get the location of the radar
//        setRadarLocation();

//        //Refresh GPS data
//        UserLocation = gps.GetLocation ();
//        UserDirection = gps.GetDirection ();

//        // Draw the radar background
//        if (radarType != RadarTypes.Transparent)
//        {
//            Rect radarRect = new Rect(_radarCenter.x - _radarWidth / 2, _radarCenter.y - _radarHeight / 2, _radarWidth, _radarHeight);
//            GUI.DrawTexture(radarRect, radarTexture);
//        }

//        //if(area == null) area = GameObject.Find ("ARscene").GetComponent<SceneAR> ().getArea ();

//        //if(area != null)
//        foreach (POI poi in area.POIs) 
//        {
//            drawBlipFromGPS(poi);
//        }
		
		
//        // Draw user direction in center object
//        if (radarCenterActive)
//        {
//            //update orientation
//            float heading = gps.GetDirection();
			
//            if(System.Math.Abs(UserDirection-heading)>=5)
//            {
//                float yVelocity = 0f;
//                float smooth = 1.3f;
				
//                float newAngle = Mathf.SmoothDampAngle(UserDirection, heading, ref yVelocity, smooth);
				
//                GUIUtility.RotateAroundPivot(newAngle, _radarCenter);
				
//                Rect centerRect = new Rect(_radarCenter.x -centerIcon.width/2, _radarCenter.y-centerIcon.width/2, centerIcon.width, centerIcon.width);
//                GUI.DrawTexture (centerRect, centerIcon);				
				
//                Debug.Log ( gps.GetDirection() + " " + newAngle);
//            }

////			Vector2 pivotPoint = new Vector2(Screen.width / 2, Screen.height / 2);
////			GUIUtility.RotateAroundPivot(rotAngle, pivotPoint);
////			
////			if (GUI.Button(new Rect(Screen.width / 2 - 25, Screen.height / 2 - 25, 50, 50), "Rotate"))
////				rotAngle += 10;											
//        }		
//    }

//    void drawBlipFromGPS(POI poi)
//    {
//        //comment out for debugging
//        //if(UserLocation != gps.GetLocation() || radarSize != CurrentRadarSize || radarZoom != CurrentRadarZoom)
//        {
//            //CurrentRadarSize = radarSize;
//            //CurrentRadarZoom = radarZoom;

//            double distance = GPSlocation.Distance(UserLocation,new GPSlocation(poi.Latitude,poi.Longitude));
//            double direction = GPSlocation.Direction(UserLocation, new GPSlocation(poi.Latitude,poi.Longitude));

//            Texture2D BlipTexture;
//            double bX;
//            double bY;

//            // For a round radar, make sure we are within the circle 
//            if (distance <= (_radarWidth - 2) * 0.5 / radarZoom)
//            {
//                BlipTexture = _radarBlip1Texture;

//                // Get the object's offset from the centerObject
//                bX = distance * System.Math.Cos(direction*Mathf.PI/180);
//                bY = distance * System.Math.Sin(direction*Mathf.PI/180);
//            }
//            else
//            {
//                BlipTexture = _radarBlip1OoBTexture;

//                bX = ((_radarWidth - 2) * 0.5 / radarZoom)  * System.Math.Cos(direction*Mathf.PI/180);
//                bY = ((_radarWidth - 2) * 0.5 / radarZoom)  * System.Math.Sin(direction*Mathf.PI/180);
//            }

//            //Debug.Log(poi.Name + " " + distance + " " + direction + " " + bX + " " + bY);

//            // Scale the objects position to fit within the radar
//            //arp no idea why dots are painted opposite from what they should be, even though distance, direcation and X-Y pairs are correct.
//            //arp Therefore, we negate the values and that's it. TODO: Find out why this is happening.
//            bX = -bX * radarZoom; 
//            bY = -bY * radarZoom;

//            Rect clipRect = new Rect(_radarCenter.x - (float)bX - 1.5f, _radarCenter.y + (float)bY - 1.5f, 3, 3);
//            GUI.DrawTexture(clipRect, BlipTexture);
//        }

//    }

//    // Create the blip textures
//    void CreateBlipTexture(Texture2D tex, Color c)
//    {
//        Color[] cols = { c, c, c, c, c, c, c, c, c };
//        tex.SetPixels(cols, 0);
//        tex.Apply();
//    }

//    // Create a round bullseye texture
//    void CreateRoundTexture(Texture2D tex, Color a, Color b)
//    {
//        Color c = new Color(0, 0, 0);
//        int size = (int)((_radarWidth / 2) / 4);

//        // Clear the texture
//        for (int x = 0; x < _radarWidth; x++)
//        {
//            for (int y = 0; y < _radarWidth; y++)
//            {
//                tex.SetPixel(x, y, c);
//            }
//        }

//        for (int r = 4; r > 0; r--)
//        {
//            if (r % 2 == 0)
//            {
//                c = a;
//            }
//            else
//            {
//                c = b;
//            }
//            DrawFilledCircle(tex, (int)(_radarWidth / 2), (int)(_radarHeight / 2), (r * size), c);
//        }

//        tex.Apply();
//    }

//    // Draw a filled colored circle onto a texture
//    void DrawFilledCircle(Texture2D tex, int cx, int cy, int r, Color c)
//    {
//        for (int x = -r; x < r; x++)
//        {
//            int height = (int)Mathf.Sqrt(r * r - x * x);

//            for (int y = -height; y < height; y++)
//                tex.SetPixel(x + cx, y + cy, c);
//        }
//    }

//    // Figure out where to put the radar
//    void setRadarLocation()
//    {
//        // Sets radarCenter based on enum selection
//        if (radarLocation == RadarLocations.TopLeft)
//        {
//            _radarCenter = new Vector2(_radarWidth / 2, _radarHeight / 2);
//        }
//        else if (radarLocation == RadarLocations.TopCenter)
//        {
//            _radarCenter = new Vector2(Screen.width / 2, _radarHeight / 2);
//        }
//        else if (radarLocation == RadarLocations.TopRight)
//        {
//            _radarCenter = new Vector2(Screen.width - _radarWidth / 2, _radarHeight / 2);
//        }
//        else if (radarLocation == RadarLocations.Left)
//        {
//            _radarCenter = new Vector2(_radarWidth / 2, Screen.height / 2);
//        }
//        else if (radarLocation == RadarLocations.Center)
//        {
//            _radarCenter = new Vector2(Screen.width / 2, Screen.height / 2);
//        }
//        else if (radarLocation == RadarLocations.Right)
//        {
//            _radarCenter = new Vector2(Screen.width - _radarWidth / 2, Screen.height / 2);
//        }
//        else if (radarLocation == RadarLocations.BottomLeft)
//        {
//            _radarCenter = new Vector2(_radarWidth / 2, Screen.height - _radarHeight / 2);
//        }
//        else if (radarLocation == RadarLocations.BottomCenter)
//        {
//            _radarCenter = new Vector2(Screen.width / 2, Screen.height - _radarHeight / 2);
//        }
//        else if (radarLocation == RadarLocations.BottomRight)
//        {
//            _radarCenter = new Vector2(Screen.width - _radarWidth / 2, Screen.height - _radarHeight / 2);
//        }
//        else if (radarLocation == RadarLocations.Custom)
//        {
//            _radarCenter = radarLocationCustom;
//        }
//    }


}