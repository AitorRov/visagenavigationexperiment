﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NavigationArrows : MonoBehaviour {

	GameObject larrow;
	GameObject rarrow;
	double angle;

	bool showArrows;
	bool experimentStarted;
	GPSlocation NextPoiCoords;
	
		
	void Start () {
		angle = 0;
		larrow = GameObject.Find ("LeftArrow");
		rarrow = GameObject.Find ("RightArrow");

		NextPoiCoords = new GPSlocation (0,0);
		
		Input.location.Start (1f, 1f);
		Input.compass.enabled = true;	
		
		showArrows = true;
		experimentStarted = false;
	}
	
	public void ExperimentStarts(){
		experimentStarted = true;
	}
	
	public void SetNextPOIcoords(GPSlocation next)
	{
		NextPoiCoords = next;
	}
						
	void Update () {
#if UNITY_IOS
		if(experimentStarted){
		
			GPSlocation myLocation = new GPSlocation(Input.location.lastData.latitude, Input.location.lastData.longitude);
			
			//66 Gower st
			//GPSlocation gs = new GPSlocation(51.5219895,-0.1320934);
			//66GS real location
			//51.5220336914063 -0.13201242685318

			//Debug.Log ("Mylocation " + myLocation.latitude + " " + myLocation.longitude); 
			//myLocation = gs;
			//Torrington place, west from 66GS
			//GPSlocation tp2 = new GPSlocation(51.5220095,-0.1336652);
			//NextPoiCoords = tp2;
			GPSlocation.Direction(myLocation,NextPoiCoords);

			angle =  GPSlocation.Direction(myLocation,NextPoiCoords) + Input.compass.trueHeading;

			//Debug.Log ( GPSlocation.Direction(myLocation,NextPoiCoords) + " ... " + Input.compass.trueHeading ); 

		}
		else{
			angle = Input.compass.trueHeading;
		}

#else
		//it seems to work different in Android, or maybe compass wasn't properly calibrated
		angle = Input.compass.trueHeading;
#endif			
		transform.rotation = Quaternion.Euler (0, 0, (float)angle);
		larrow.gameObject.SetActive(showArrows && (angle -90 < 180));
		rarrow.gameObject.SetActive(showArrows && (angle -90 > 180));

	}
}
