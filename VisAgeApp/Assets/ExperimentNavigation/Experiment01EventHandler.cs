﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class Experiment01EventHandler : MonoBehaviour {
	
	Area area;
	Experiment01 experiment01;
	bool experimentStarted = false;
	
	public Sprite ExperimentGreenIcon;
	public Sprite ExperimentWhiteIcon;
	public Sprite ExperimentRedIcon;	
	
	bool ShowImage;
	bool ShowArrows;
	RawImage imagePreview;
	
	ScreenSaver sSaver;
	bool  screenOn = true;
	float timer = 0;
	float timerMax = 5;	
	float timeScreenOn = 0;
	
	void Start () 
	{	
		area = GameObject.Find("SceneMain").GetComponent<SceneVisage>().area;
		
		experiment01 = new Experiment01(area);
		experiment01.GreenIcon = ExperimentGreenIcon;
		experiment01.WhiteIcon = ExperimentWhiteIcon;
		experiment01.RedIcon   = ExperimentRedIcon;			
		
		ShowImage = true;
		ShowArrows = true;
		
		GameObject go = GameObject.Find("ImagePreview");
		imagePreview = go.GetComponent<RawImage>();
		imagePreview.enabled = false;
		
		timeScreenOn = 0;
		
		sSaver = gameObject.GetComponent<ScreenSaver>();
	}
	
	void Update()
	{
		if(experiment01.IsTrackerTimeOut())
		{
			TrackerTimeOutShow();
		}
		
		timer += Time.deltaTime;

		if(experimentStarted && screenOn)
		{
			experiment01.ScreenIsOn();
		}
						
		if(timer > timerMax)
		{
			sSaver.StartFade(new Color(0,0,0,1), 1f);	
			screenOn = false;
		}
		
		if(Input.touchCount > 0 || Input.GetMouseButton(0))
		{
			timer = 0;
			sSaver.StartFade(new Color(0,0,0,0), 0.5f);	
			screenOn = true;
		}		
		
	}
	
	public void StartExperiment()
	{
		GameObject.Find ("PanelExperiment").SetActive(false);
		
		experimentStarted = true;
		
		experiment01.Start(ShowArrows, ShowImage);
	}
	
	public void TrackerFound(int trackerId)
	{
		experiment01.TrackerFound(trackerId);
	}	
	
	public void TrackerTimeOutShow()
	{
		LoadImage(Path.Combine(Application.persistentDataPath,SceneTools.AreaNameDefault()+"/TimeOut.png"), 1.0f);
	}
	
	public void TrackerPreviewShow(POI poi)
	{
		if(ShowImage)
		{
			//arp show the current POI preview only
			//string ImagePath = Path.Combine(Application.persistentDataPath,SceneTools.AreaNameDefault()+"/StreamingAssets/"+poi.ImageTarget);
			int index = experiment01.currentPOI > 0 ? experiment01.currentPOI - 1 : 0;
			LoadImage(Path.Combine(Application.persistentDataPath,SceneTools.AreaNameDefault()+"/StreamingAssets/"+ area.POIs[index].ImageTarget), 0.6f);
		}
	}
	
	void LoadImage(string imagePath, float alpha)
	{
		imagePreview.enabled = true;
		
		Texture2D tex = new Texture2D(1,1);
		tex.LoadImage(System.IO.File.ReadAllBytes(imagePath));
		imagePreview.texture = tex;
		//imagePreview.rectTransform.sizeDelta = new Vector2(tex.width/3, tex.height/3);
		
		float h,w;
		if(tex.width > tex.height)
		{
			w = Screen.width * 0.5f;
			h = tex.height * w/tex.width;
		}
		else
		{
			h = Screen.height * 0.5f;
			w = tex.width * h/tex.height;
		}
		
		Debug.Log(Screen.width+"x"+Screen.height + "W: " + w + "H: " + h);
				
		imagePreview.rectTransform.sizeDelta = new Vector2(w,h);
		imagePreview.rectTransform.position = new Vector2(Screen.width/2,Screen.height/2);
		imagePreview.canvasRenderer.SetAlpha(alpha);
	}
	
	public void TrackerPreviewHide()
	{
		imagePreview.enabled = false;
	}
	
	public void ToggleImages()
	{
		ShowImage = ShowImage ? false : true;
	}
	
	public void ToggleArrows()
	{
		ShowArrows = ShowArrows ? false : true;
	}
}
