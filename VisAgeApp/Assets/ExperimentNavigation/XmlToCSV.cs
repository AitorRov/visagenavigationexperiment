﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;

public class XmlToCSV : MonoBehaviour {

	string path = "c:\\Users\\Gene\\Dropbox\\VisAge\\2015 05 Experiment Navigation and Safety\\Data\\";
	Experiment01Data data;
	
	DateTime lastTime;
	int poisfound;
	
	string header;
	string text;
	
	bool firstFile = true;

	void Start () 
	{
		
	
		string[] files = Directory.GetFiles(path, "*.xml", SearchOption.TopDirectoryOnly);
		
		
		
		foreach(string file in files)
		{
			
			data = Experiment01Data.Load(file);
			
			TimeSpan timeelapsed = data.end.Subtract(data.start);
			int secondselapsed = (int)timeelapsed.TotalSeconds;
			
			// +","+ 
			if(firstFile) header = "ParticipantId, ArrowsOn, ImagesOn, TimeElapsed, TimeScreenOn,PoisFound,"; 
			
			int arrowsOn = data.arrowsOn? 1 : 0;
			int imagesOn = data.imagesOn? 1 : 0;
			
			text += data.participantId +","+ arrowsOn +","+ imagesOn +","+ secondselapsed.ToString() +","+ data.timeScreenOn+",";
			
			lastTime = data.start;
			string textpois="";
			int poisfound = 0; 
			
			foreach (ExperimentPOI poi in data.pois)
			{
				if(firstFile) header += poi.name + ".Found" + "," + poi.name + ".TimeNeeded" + ",";
				
				TimeSpan timeNeeded = poi.time.Subtract(lastTime);
				int secondsNeeded = (int)timeNeeded.TotalSeconds;
				
				textpois += ((poi.timeOut==1)?0:1).ToString()+"," + secondsNeeded  +",";
				
				poisfound = poisfound + Mathf.Abs(poi.timeOut -1);
				lastTime = poi.time;
			}
			
			text += poisfound +","+ textpois + "\n";
			
			//Debug.Log (data.participantId);
			firstFile = false;
		}
		
		header += "\n";
		
		
		System.IO.File.WriteAllText(Path.Combine (path,"output.csv"), header + text);
		
		Debug.Log ("done.");
	}
	
}
