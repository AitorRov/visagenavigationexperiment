﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Compass : MonoBehaviour 
{
	Image circle;
	bool experimentStarted;
	GPSlocation nextPoiCoords;	
	
 	Text textdebug;
 	
 	public float SimTrueHeading = 0;
									
	void Start()
	{
		circle = GameObject.Find (gameObject.GetPathInHierarchy() + "/Circle").GetComponent<Image>();
		
		experimentStarted = false;
		nextPoiCoords = new GPSlocation (0,0);
		
		Input.location.Start (1f, 1f);
		Input.compass.enabled = true;	
		
 		if(GameObject.Find ("Canvas/TextDebug"))
 			textdebug = GameObject.Find ("Canvas/TextDebug").GetComponent<Text>();		
	}
	
	void Update()
	{
		GPSlocation location = GetCurrentLocation();
		double direction 	 = GetDirection (location);
		float distance 		 = (float)GPSlocation.Distance(GetCurrentLocation(), nextPoiCoords);
		float width 		 = GetAccuracy() >= distance ? 180 : Mathf.Atan2(GetAccuracy(), distance);

		transform.rotation = Quaternion.Euler (0, 0, (float)direction);
		circle.fillAmount  = width/Mathf.PI; //it's actually width*2 / PI*2
		//TODO: fix rotation in GameObject transform. It should be zero, the texture needs to be rotated
		circle.transform.localRotation = Quaternion.Euler (0, 0, 180 + Mathf.Rad2Deg * width);
		
 		if(textdebug)
 		{
 			
 			string TheText = "acc: " + GetAccuracy() + " d: " + distance + " fill: " + circle.fillAmount +  "\n" +
 				"direction : " + direction + "\n" +
 				"poicoords: " + nextPoiCoords.latitude + ", " + nextPoiCoords.longitude;
 							 
 			textdebug.text = TheText;
      	}		
	}
	
	public void SetNextPOIcoords(GPSlocation next)
	{
		nextPoiCoords = next;
	}	
	
	public void ExperimentStarts()
	{
		experimentStarted = true;
	}	
	
	public float GetAccuracy()
	{
		if(UnityEngine.Input.location.status == LocationServiceStatus.Running)
		{
			return Input.location.lastData.horizontalAccuracy ;
		}

		return SceneTools.DefaultGPSaccuracy();
	}	
	
	public GPSlocation GetCurrentLocation()
	{
		if(UnityEngine.Input.location.status == LocationServiceStatus.Running)
		{		
			return new GPSlocation(Input.location.lastData.latitude, Input.location.lastData.longitude);
		}
		
		return SceneTools.DefaultGPSlocation();
	}
	
	public double GetDirection(GPSlocation location)
	{
		
		float trueHeading = Input.location.status  == LocationServiceStatus.Running ? Input.compass.trueHeading : SimTrueHeading;

        return trueHeading;
	
	}
}
