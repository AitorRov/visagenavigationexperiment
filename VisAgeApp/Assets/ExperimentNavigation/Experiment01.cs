﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class Experiment01 
{
	Area currentArea;
	public int  currentPOI;
	int  numPOIS;
	
	bool hasStarted;
	Experiment01Data data;
	
	public Sprite GreenIcon;
	public Sprite WhiteIcon;
	public Sprite RedIcon;
	
	DateTime currentTrackerTimeOut;
	int[] trackerTimeLimit = {120,120,120,120,90,90,90,60,60,1000}; //time limit to find each tracker, in seconds
	//int[] trackerTimeLimit = {5,5,5,5,5,5,5,5,5,555}; //time limit to find each tracker, in seconds
	
	float timeScreenOn = 0;
	
	Text textCounter;
	//NavigationArrows ArrowCompassScript;
	Compass compass;
	
	public Experiment01(){}
	public Experiment01(Area area)
	{
		currentArea = area;
		numPOIS = area.POIs.Length;
		
		textCounter = GameObject.Find("Canvas/TextCounter").GetComponent<Text>();
		Debug.Log ("Experiment created with " + numPOIS + " pois");

		//ArrowCompassScript = GameObject.FindObjectOfType<NavigationArrows>();
		compass = GameObject.FindObjectOfType<Compass>();
	}
	
	public void Start(bool ArrowsOn, bool ImagesOn)
	{
		data 					= new Experiment01Data();
		data.area   			= currentArea.Name;
		data.participantId 		= "00";
		data.arrowsOn 			= ArrowsOn;
		data.imagesOn 			= ImagesOn;
		data.numPOIs			= numPOIS;
		data.pois				= new ExperimentPOI[numPOIS];
		data.start  			= System.DateTime.Now;

		currentPOI  = 0 ;
		hasStarted  = true;

		TrackerNext(0);

		if(compass)
		{
			//ArrowCompassScript.ExperimentStarts ();
			//ArrowCompassScript.SetNextPOIcoords(currentArea.FindPoi(currentPOI).GetGPSlocation());		
			compass.ExperimentStarts();
			compass.SetNextPOIcoords(currentArea.FindPoi(currentPOI).GetGPSlocation());
		}
	
		Debug.Log ("Experiment Started with " + numPOIS + " pois");
	}
	
	void SaveData()
	{
		data.end = System.DateTime.Now;
		data.timeScreenOn = timeScreenOn;
		data.Save();
		data.Send();
	}

	public void TrackerFound(int trackerId) 
	{
		if(hasStarted && trackerId == currentPOI) TrackerNext(0);
	}
	
	public bool IsTrackerTimeOut()
	{
		if(hasStarted && currentTrackerTimeOut < System.DateTime.Now)
		{
			TrackerNext(1);
			return true;
		}
		
		return false;
	}
	
	public void ScreenIsOn()
	{
		timeScreenOn += Time.deltaTime;
		//Debug.Log (timeScreenOn);
	}
			
	void TrackerNext(int timeOut)
	{
		Debug.Log ("Last tracker: " + currentPOI);
		
		if(currentPOI > 0)
		{	//add log entry of previous tracker, except for calling the first poi
			ExperimentPOI newpoi = new ExperimentPOI();
			newpoi.Id   = currentArea.POIs[currentPOI-1].Id;
			newpoi.name = currentArea.POIs[currentPOI-1].Name;
			newpoi.time = System.DateTime.Now;
			newpoi.timeOut = timeOut;
			data.pois[currentPOI-1] = newpoi;
		}
		
		currentPOI = currentPOI + 1;
		
		if(currentPOI > numPOIS)
		{	//it's the end of the route
			SaveData();
			hasStarted = false;
			textCounter.text = "Route finished.";
			Debug.Log ("Experiment ended.");
		}
		else 
		{	//update next tracker information
		
			//compass
			//ArrowCompassScript.SetNextPOIcoords(currentArea.FindPoi(currentPOI).GetGPSlocation());
			if(compass) compass.SetNextPOIcoords(currentArea.FindPoi(currentPOI).GetGPSlocation());
			textCounter.text = (currentPOI-1).ToString() + " out of " +numPOIS;
			
			//time limit
			currentTrackerTimeOut = System.DateTime.Now.AddSeconds(trackerTimeLimit[currentPOI-1]);
//			Debug.Log (trackerTimeLimit[currentPOI-1] + " " + currentTrackerTimeOut);
		}	
		
		foreach (POI poi in currentArea.POIs)
		{	//update map icon
			int cosId = poi.GetCosId ();
			
			if(cosId < currentPOI)		 poi.GetMapObj().GetComponent<Image>().sprite = RedIcon;
			else if(cosId == currentPOI) poi.GetMapObj().GetComponent<Image>().sprite = WhiteIcon;
			else if(cosId > currentPOI)  poi.GetMapObj().GetComponent<Image>().sprite = GreenIcon;
		}			
	}
	
}






