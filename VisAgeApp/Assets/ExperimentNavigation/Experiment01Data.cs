﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

[XmlRoot("Experiment01Data")]
public class Experiment01Data
{
	public string area;
	public int numPOIs;
	
	public string participantId;
	public bool arrowsOn;
	public bool imagesOn;
	
	public DateTime start;
	public DateTime end;
	public float timeScreenOn;

	[XmlArray("Pois"),XmlArrayItem("Poi")]
	public ExperimentPOI[] pois;	
	
	string path;
	
	public void Save()
	{
		path = Path.Combine (Application.persistentDataPath, start.ToString("yyyyMMdd_HHmmss") + "_" + participantId + ".xml");
		XmlSerializer serializer = new XmlSerializer(typeof(Experiment01Data));
		using(var stream = new FileStream(path, FileMode.Create))
		{
			serializer.Serialize(stream, this);
		}
	}	
	
	public static Experiment01Data Load(string filename)
	{
		XmlSerializer serializer = new XmlSerializer(typeof(Experiment01Data));
		FileStream fs = new FileStream(filename, FileMode.Open);
		XmlReader reader = XmlReader.Create(fs);
		
		Experiment01Data data;
		data = (Experiment01Data)serializer.Deserialize(reader);
		fs.Close ();
		
		return data;
	}
	
	public void Send()
	{
		MailMessage mail = new MailMessage();
		mail.To.Add("aitorr@gmail.com");
		mail.Subject = "Experiment 01 - participant ";// + participantId.ToString();
		mail.Body = "Data";		
	
		//last XML file only
		mail.Attachments.Add(new Attachment (path));
		
		SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential("hmdlabucl@gmail.com", "willeatsbananas") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) 
		{ return true; };

		smtpServer.Send(mail);
		Debug.Log("Email sent.");

	}		
}

public class ExperimentPOI
{
	public string Id;
	public string name;
	public DateTime time;
	public int   timeOut;
}
