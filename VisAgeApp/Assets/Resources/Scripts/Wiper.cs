using UnityEngine;
using System.Collections;


/// <summary>
/// Class that handles input and clips (cuts a hole in) the texture of the GameObject to which it is attached.
/// The GameObject needs to have a collider set and use the material mHoleMaterial with "Custom/Depth Holes" shader.
/// Made with the help from http://unitycoder.com/blog/2011/08/10/painting-holes-with-shader-v2-0/ 
/// </summary>
public class Wiper : MonoBehaviour {
	
	#region PRIVATE VARIABLES
	private Texture2D baseTex;
	private bool mClearFlag;
	private float mCleared;
	#endregion

	void Start () {
		initTexture();
		mClearFlag = false;
		mCleared = 0f;
	
	}
	
	// Update is called once per frame
	void Update () {
		//Do nothing if there is no dragging input
		if(!(Input.touchCount > 0 && Input.GetTouch(0).phase==TouchPhase.Moved)){
			return;	
		}
		
		Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
		RaycastHit hit;
		
		//Do nothing if nothing was hit
		if(!Physics.Raycast (ray, out hit, 1000.0f)){
			return;
		}
		
		//Do nothing if a masking plane was not hit
		if(hit.transform.tag != "Masking Plane"){
			return;
		}
		
		//Get coordinates where ray hit
		Vector2 pixelUV = hit.textureCoord;
				
		//Get the _SliceGuide texture for the hit object's material
		Texture2D tex = (Texture2D)hit.transform.GetComponent<Renderer>().material.GetTexture("_SliceGuide");
		
		//Only count clearance percentage while it is not clear, for better performance
		if(!mClearFlag){
		mCleared = PercentageClear(tex);
		}
		if(mCleared > 0.3f){
			mClearFlag = true;	
		}
		
		pixelUV.x *= tex.width;
		pixelUV.y *= tex.height;
		Debug.Log ("hit "+hit.transform.name+" at ("+(int)pixelUV.x+","+(int)pixelUV.y+")");
		
		//Paint with black to the clipping texture so the value turns negative and is clipped (in the shader)
		paintCircle (pixelUV,4,Color.black,tex);
	}
	
	/// <summary>
	/// Inits the texture.
	/// </summary>
	private void initTexture(){
		baseTex = new Texture2D(16,16);
		GetComponent<Renderer>().material.SetTexture("_SliceGuide",baseTex);
		
		// Fill the texture with white (you could also paint it black, then draw with white)
		for (int y = 0; y < baseTex.height; ++y) 
		{
			for (int x = 0; x < baseTex.width; ++x) 
			{
				baseTex.SetPixel (x, y, Color.white);
			}
		}
		// Apply all SetPixel calls
		baseTex.Apply();
	}
	
	//Calls setPixel in a circle around the pixel coordinate passed
	public void paintCircle(Vector2 pixel, int width, Color col, Texture2D tex){
		float cX = pixel.x;
		float cY = pixel.y;
		float radius = width/2;
		float stX = pixel.x-width/2.0f;
		float stY = pixel.y-width/2.0f;

		for(float x = stX; x<(stX+width);x++){
			for(float y = stY; y<(stY+width);y++){
				if((Mathf.Pow (x-cX,2)+Mathf.Pow (y-cY,2)) < Mathf.Pow (radius,2)){
					tex.SetPixel ((int)x,(int)y,col);
					//Debug.Log ("Painted at (" +x+","+y+")");
				}	
			}
		}
		tex.Apply ();
	}
	
	//Resets the gameObjects material and layer
	public void Reset(){
		GameObject thisObject = transform.gameObject;
		thisObject.GetComponent<Renderer>().material = (Material)Resources.Load ("Materials/mHoleMaterial",typeof(Material));
		thisObject.layer = 2;
		mClearFlag = false;
		initTexture();
	}
	
	/// <summary>
	/// Returns the percentage of black pixels in a texture
	/// </param>
	private float PercentageClear(Texture2D tex){
		int clearCount = 0;
		for(int x=0; x<tex.width;x++){
			for(int y=0; y<tex.height;y++){
				if(tex.GetPixel (x,y).grayscale < 0.5f){
					clearCount++;	
				}
			}
		}
		return (float)clearCount/(tex.width*tex.height);
	}
	
	//Returns true if the texture is 30% black
	public bool IsClear(){
		return mClearFlag;
	}
	
}
