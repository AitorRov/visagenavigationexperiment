﻿using UnityEngine;
using System.Collections;

public class ControlContentContainers : MonoBehaviour
{
    GUIStyle mBoxStyle;
    GUIStyle mLabelStyle;
    GUIStyle mLabelSelectStyle;
	float BoxWidth;
	float BoxHeight;
    float BoxOffsetX;
    float BoxOffsetY;
	float ButtonWidth;
	float ButtonHeight;
    float ButtonOffsetX;
    float ButtonOffsetXstep;
    float ButtonOffsetY;

    public  POI poi;
    int NumCCs;
    string CurrentCC;
    bool first;
    bool visible;

	public ControlContentContainers()
    {
        first = true;
        visible = false;
	}

    public void SetPoi(POI newpoi)
    {
        poi = newpoi;
    }
    
	private void initGUI() //initialize once
	{
		NumCCs = poi.ContentContainers.Length;
		CurrentCC = poi.ContentContainers[0].Name;

		mBoxStyle = new GUIStyle(GUI.skin.FindStyle("box"));
		mBoxStyle.normal.background = Resources.Load("Textures/boxblack") as Texture2D;
		mBoxStyle.border = new RectOffset(10, 10, 10, 10);
		
		mLabelStyle = new GUIStyle(GUI.skin.FindStyle("label"));
        mLabelStyle.alignment = TextAnchor.MiddleCenter;
        mLabelStyle.normal.textColor = Color.gray;

        mLabelSelectStyle = new GUIStyle(GUI.skin.FindStyle("label"));
        mLabelSelectStyle.alignment = TextAnchor.MiddleCenter;
        mLabelSelectStyle.normal.background = Resources.Load("Textures/boxwhite") as Texture2D;
        mLabelSelectStyle.normal.textColor = Color.black;
	}
	
	private void SetupGUI() //initialize every frame
	{
		BoxWidth                    = Screen.width  * 0.66f;
		BoxHeight                   = Screen.height * 0.05f;
        BoxOffsetX                  = Screen.width  * 0.165f;
        BoxOffsetY                  = Screen.height * 0.9f;
        ButtonWidth                 = BoxWidth / (NumCCs);
        ButtonHeight                = BoxHeight;
        ButtonOffsetX               = 0f;
        ButtonOffsetXstep           = ButtonWidth;
        ButtonOffsetY               = BoxHeight / 80f;
        mLabelStyle.fontSize        = (int)(Screen.height / 43);
        mLabelSelectStyle.fontSize  = (int)(Screen.height / 35);        
    }

    public void OnGUI()
    {
        if (visible)
        {
            if (first)
            {
                initGUI();
                first = false;
            }

            SetupGUI();

            GUI.BeginGroup(new Rect(BoxOffsetX, BoxOffsetY, BoxWidth, BoxHeight), mBoxStyle);

            foreach (ContentContainer cc in poi.ContentContainers)
            {
                //Debug.Log(BoxOffsetX + " " + BoxOffsetY + " " + BoxWidth + " " + BoxHeight);
                //Debug.Log(ButtonOffsetX+" " + ButtonOffsetY+" " + ButtonWidth+" " + ButtonHeight);
                if (GUI.Button(new Rect(ButtonOffsetX, ButtonOffsetY, ButtonWidth, ButtonHeight), cc.Name, CurrentCC == cc.Name ? mLabelSelectStyle : mLabelStyle))
                {
                    CurrentCC = cc.Name;

                    foreach (Transform t in transform)
                    {
                        t.gameObject.SetActive(t.name == CurrentCC.ToString());
                    }
                }
                ButtonOffsetX += ButtonOffsetXstep;
            }

            GUI.EndGroup();
        }
    }

    public void Update()  //TODO: This should be optimized to not be called every frame
    {
        foreach (Transform t in transform)
        {
            ControlAudio[] ControlAudios = t.GetComponentsInChildren<ControlAudio>();
            foreach (ControlAudio audio in ControlAudios)
            {
                audio.enabled = audio.isPlaying() || (visible && (t.name == CurrentCC.ToString()));
            }

            GUIText[] texts = t.GetComponentsInChildren<GUIText>();
            foreach (GUIText text in texts)
            {
                text.enabled = visible && (t.name == CurrentCC.ToString());
            }
        }
    }

    public void EnableRendering(bool enable)
    {
        visible = enable;
    }

}


